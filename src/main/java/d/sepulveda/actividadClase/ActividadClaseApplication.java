package d.sepulveda.actividadClase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActividadClaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActividadClaseApplication.class, args);
	}

}
